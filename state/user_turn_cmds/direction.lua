
local DIR = require 'common.direction'
local Vec = require 'common.vec'

-- Forward declaration of auxiliary functions
local _blockingEntity, _movementTarget, _actOnEntity, _move

local function _handleDirection(dir, userturn)
  local session = userturn.client.session
  local request
  if dir ~= DIR.NONE then
    local blocking_entity_idx = _blockingEntity(session, dir)
    if blocking_entity_idx then
      request = _actOnEntity(session, blocking_entity_idx)
    end
    if not request and not blocking_entity_idx then
      request = _move(session, dir)
    end
  end
  return request
end

function _blockingEntity(session, dir)
  local position = _movementTarget(session, dir)
  for _, idx in session:entitiesAt(position) do
    if not session:entityPassable(idx) then
      return idx
    end
  end
end

function _movementTarget(session, dir)
  local player_idx = session:playerIndex()
  local player_position = session:entityPosition(player_idx)
  local movement = Vec.fromDir(dir)
  return player_position + movement
end

function _actOnEntity(session, idx)
  local primary_action = session:primaryActionOn(idx)
  if primary_action then
    return {
      TryAction = {
        primary_action, {
          Self = session:playerID(),
          Target = idx
        }
      }
    }
  end
end

function _move(session, dir)
  return {
    TryAction = {
      "move", {
        Self = session:playerID(),
        Target = session:playerID(),
        Dir = dir
      }
    }
  }
end

return _handleDirection

