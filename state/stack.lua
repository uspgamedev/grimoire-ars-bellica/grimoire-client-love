
local CLASS = require 'pl.class'

local Stack = CLASS()

function Stack:_init()
  self.top = 0
  self.states = {}
  self.registered = {}
end

function Stack:getState(name)
  if name then
    local state = self.registered[name] if not state then
      state = require('state.' .. name) ()
      self.registered[name] = state
    end
    return state
  end
end

function Stack:getCurrentState()
  return self:getState(self.states[self.top])
end

function Stack:push(statename, message)
  local current = self:getCurrentState() if current then
    current:onSuspend()
  end
  self.top = self.top + 1
  self.states[self.top] = statename
  self:getCurrentState():onEnter(message)
end

function Stack:pop(message)
  self:getCurrentState():onLeave()
  self.states[self.top] = false
  self.top = self.top - 1
  local current = self:getCurrentState() if current then
    current:onResume(message)
  end
end

function Stack:update(client, dt)
  if self.top > 0 then
    local change = self:getCurrentState():onUpdate(client, dt)
    if change then
      local key, value = next(change)
      if key == 'pop' then
        self:pop(value)
      elseif key == 'push' then
        self:push(value.to, value.message)
      else
        error("Unknown stack change: " .. key)
      end
    end
  end
end

return Stack


