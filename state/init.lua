
local CLASS = require 'pl.class'

local State = CLASS()

-- luacheck: no unused, no self

function State:_init()
  -- empty
end

function State:onEnter(params)
  -- abstract method
end

function State:onLeave()
  -- abstract method
end

function State:onSuspend()
  -- abstract method
end

function State:onResume(message)
  -- abstract method
end

function State:onUpdate(dt)
  -- abstract method
end

function State.pop(message)
  return { pop = message }
end

function State.push(statename, message)
  return { push = { to = statename, message = message } }
end

return State


