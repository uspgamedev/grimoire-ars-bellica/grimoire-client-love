
local DIR   = require 'common.direction'
local Match = require 'common.match'
local State = require 'state'
local CLASS = require 'pl.class'

local ChooseInteraction = CLASS(State)

local USERCMD = Match()

function ChooseInteraction:_init()
  self.actions = nil
  self.selected = nil
  self:super()
end

function ChooseInteraction:onEnter(message)
  self.actions = {}
  self.selected = 1
  for _, action in message.session:actionsOn(message.entity_id) do
    local n = #self.actions + 1
    for i, action_info in ipairs(self.actions) do
      if action_info.name == action.name then
        n = i
        break
      end
    end
    local info = self.actions[n] or { name = action.name, bindings = {} }
    table.insert(info.bindings, action)
    self.actions[n] = info
  end
end

function ChooseInteraction:onUpdate(client, _)
  local menu_window = client.ui.menu_window
  local command = client.ui.control:getCommand()
  local change = USERCMD(command, self, menu_window)
  local chosen_action
  menu_window:text("Choose action:")
  for i, action in ipairs(self.actions) do
    if menu_window:selectable(action.name, i == self.selected) then
      chosen_action = action.bindings[1]
    end
  end
  if chosen_action then
    return State.pop { action = chosen_action }
  elseif change then
    return change
  end
end

function USERCMD.onMove(dir, state, _)
  if dir == DIR.UP then
    state.selected = math.max(1, state.selected - 1)
  elseif dir == DIR.DOWN then
    state.selected = math.min(#state.actions, state.selected + 1)
  end
end

function USERCMD.onConfirm(_, _, menu_window)
  menu_window:confirm()
end

function USERCMD.onCancel(_, _, _)
  return State.pop {}
end

return ChooseInteraction

