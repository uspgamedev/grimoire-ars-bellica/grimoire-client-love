
local Match = require 'common.match'
local Vec   = require 'common.vec'

local CLASS = require 'pl.class'

local Inspect = CLASS(require 'state')

local USERCMD = Match()

function Inspect:_init()
  self:super()
  self.map_window = nil
  self.cursor_pos = nil
end

function Inspect:onEnter(message)
  self.cursor_pos = message.client.session:playerPosition()
end

function Inspect:onUpdate(client, _) -- luacheck: no self
  client.ui.map_window:setCursor(self.cursor_pos:clone())
  local entity_inspected = nil
  for _, entity_idx in client.session:entitiesAt(self.cursor_pos) do
    entity_inspected = entity_idx
  end
  client.ui.description_window:setEntity(entity_inspected)
  local command = client.ui.control:getCommand()
  return USERCMD(command, client, self)
end

function USERCMD.onMove(dir, _, state)
  state.cursor_pos = state.cursor_pos + Vec.fromDir(dir)
end

function USERCMD.onInspect(_, client, _)
  client.ui.map_window:setCursor(nil)
  local player_idx = client.session:playerIndex()
  client.ui.description_window:setEntity(player_idx)
  return { pop = {} }
end

return Inspect

