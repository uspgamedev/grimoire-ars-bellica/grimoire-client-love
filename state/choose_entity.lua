
local DIR   = require 'common.direction'
local Match = require 'common.match'
local State = require 'state'
local CLASS = require 'pl.class'

local ChooseEntity = CLASS(State)

local USERCMD = Match()

function ChooseEntity:_init()
  self.entities = nil
  self.selected = nil
  self.action = nil
  self:super()
end

function ChooseEntity:onEnter(message)
  self.action = nil
  self.selected = 1
  self.entities = message.session:allEntitiesWithActions()
end

function ChooseEntity:onUpdate(client, _)
  if self.action then
    return State.pop { action = self.action }
  end

  local menu_window = client.ui.menu_window
  local command = client.ui.control:getCommand()
  local change = USERCMD(command, self, menu_window)
  local chosen_entity
  menu_window:text("Choose entity:")
  for i, id in ipairs(self.entities) do
    local name = client.session:entityName(id) .. '#' .. id
    if menu_window:selectable(name, i == self.selected) then
      chosen_entity = id
    end
  end
  if chosen_entity then
    return State.push('choose_interaction', {
      session = client.session,
      entity_id = chosen_entity
    })
  elseif change then
    return change
  end
end

function ChooseEntity:onResume(message) -- luacheck: no self
  self.action = message.action
end

function USERCMD.onMove(dir, state, _)
  if dir == DIR.UP then
    state.selected = math.max(1, state.selected - 1)
  elseif dir == DIR.DOWN then
    state.selected = math.min(#state.entities, state.selected + 1)
  end
end

function USERCMD.onConfirm(_, _, menu_window)
  menu_window:confirm()
end

function USERCMD.onCancel(_, _, _)
  return State.pop {}
end

return ChooseEntity

