

local DIR   = require 'common.direction'
local Match = require 'common.match'
local Vec   = require 'common.vec'
local State = require 'state'

local CLASS = require 'pl.class'
local JSON  = require 'dkjson'

local UserTurn = CLASS(State)

local USERCMD = Match()

function UserTurn:_init()
  self:super()
  self.action = nil
end

function UserTurn:onEnter(_)
  self.action = nil
end

function UserTurn:onUpdate(client, _) -- luacheck: no self
  if self.action then
    local request = client.session:targetedActionRequest(self.action)
    local coded = JSON.encode(request) .. '\n'
    client.server_stream:send(coded)
    return State.pop {}
  end
  local player_idx = client.session:playerIndex()
  client.ui.description_window:setEntity(player_idx)
  local command = client.ui.control:getCommand()
  return USERCMD(command, client)
end

function UserTurn:onResume(message) -- luacheck: no self
  self.action = message.action
end

function USERCMD.onMove(dir, client)
  local session = client.session
  local request = nil
  if dir ~= DIR.NONE then
    local player_pos = session:playerPosition()
    local move = Vec.fromDir(dir)
    local blocking_entity_idx = session:blockingEntityAt(player_pos + move)
    if blocking_entity_idx then
      local primary_action = session:primaryActionOn(blocking_entity_idx)
      if primary_action then
        request = session:targetedActionRequest(primary_action)
      end
    end
    if not blocking_entity_idx then
      request = session:moveRequest(dir)
    end
  end
  if request then
    local coded = JSON.encode(request) .. '\n'
    client.server_stream:send(coded)
    return State.pop {}
  end
end

function USERCMD.onInteractMove(dir, client)
  local session = client.session
  if dir ~= DIR.NONE then
    local player_pos = session:playerPosition()
    local move = Vec.fromDir(dir)
    local target_position = player_pos + move
    local entity_id = session:firstEntityAt(target_position)
    if entity_id then
      return State.push('choose_interaction', {
        session = session,
        entity_id = entity_id
      })
    end
  end
end

function USERCMD.onAct(_, client)
  return State.push('choose_entity', {
    session = client.session,
  })
end

function USERCMD.onAttackMove(dir, client)
  local session = client.session
  local request = nil
  if dir ~= DIR.NONE then
    request = session:defaultActionRequest(dir)
  end
  if request then
    local coded = JSON.encode(request) .. '\n'
    client.server_stream:send(coded)
    return State.pop {}
  end
end

function USERCMD.onWait(_, client)
  local session = client.session
  local request = session:actionRequest("wait")
  local coded = JSON.encode(request) .. '\n'
  client.server_stream:send(coded)
  return State.pop {}
end

function USERCMD.onInspect(_, client)
  return State.push('inspect', { client = client })
end

return UserTurn

