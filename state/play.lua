
local Session = require 'client.session'
local Match   = require 'common.match'
local State   = require 'state'
local CLASS   = require 'pl.class'
local JSON    = require 'dkjson'

local Play = CLASS(State)

local SERVER_RESPONSE = Match()

function Play:_init()
  self:super()
  self.leave = false
end

function Play:onEnter()
  self.leave = false
end

function Play:onResume(message)
  self.leave = message.quit
end

function Play:onUpdate(client, _)
  if self.leave then
    return State.pop {}
  end
  local response = client.server_stream:receive()
  if response then
    print(response)
    return SERVER_RESPONSE(JSON.decode(response), client)
  end
end

function SERVER_RESPONSE.onStateChanged(session_state, client)
  client.session = Session(session_state)
end

function SERVER_RESPONSE.onNoPlayer(_, _)
  return State.push('dead', {})
end

function SERVER_RESPONSE.onActionPending(motive, _)
  if type(motive) == 'string' then
    print(motive)
  else
    print(next(motive))
  end
  return State.push('user_turn', {})
end

return Play

