
return {
  [0] = 'NONE', 'UP', 'DOWN', 'LEFT', 'RIGHT', 'UPLEFT', 'UPRIGHT', 'DOWNLEFT',
  'DOWNRIGHT',
  NONE = 0,
  UP = 1,
  DOWN = 2,
  LEFT = 3,
  RIGHT = 4,
  UPLEFT = 5,
  UPRIGHT = 6,
  DOWNLEFT = 7,
  DOWNRIGHT = 8,
}

