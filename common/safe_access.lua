
local CLASS = require 'pl.class'

local SafeAccess = CLASS()

function SafeAccess:__index(k) -- luacheck: no self
  return error("invalid field " .. k)
end

return SafeAccess

