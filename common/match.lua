
local Match = require 'pl.class' ()

local function _NIL() end

function Match:_init() -- luacheck: no self
  -- nothing
end

function Match:__call(case, ...)
  if case then
    local case_type, case_value = next(case)
    return (self['on' .. case_type] or _NIL)(case_value, ...)
  end
end

return Match

