
local DIR = require 'common.direction'

local Vec = require 'pl.class' ()

function Vec:_init(x, y, z)
  self.x = x or 0
  self.y = y or 0
  self.z = z or 0
end

function Vec:clone()
  return Vec(self.x, self.y, self.z)
end

local DIR2VEC = {
  [DIR.NONE] = Vec(),
  [DIR.UP] = Vec(0, -1),
  [DIR.DOWN] = Vec(0, 1),
  [DIR.LEFT] = Vec(-1, 0),
  [DIR.RIGHT] = Vec(1, 0),
  [DIR.UPLEFT] = Vec(-1, -1),
  [DIR.UPRIGHT] = Vec(1, -1),
  [DIR.DOWNLEFT] = Vec(-1, 1),
  [DIR.DOWNRIGHT] = Vec(1, 1),
}

function Vec.fromDir(dir)
  return DIR2VEC[dir]
end

function Vec:__tostring()
  return "(" .. self.x .. ", " .. self.y .. ", " .. self.z .. ")"
end

function Vec:set(x, y, z)
  self.x = x or self.x
  self.y = y or self.y
  self.z = z or self.z
end

function Vec:get()
  return self.x, self.y, self.z
end

function Vec:__add(other)
  return Vec(self.x + other.x, self.y + other.y, self.z + other.z)
end

function Vec:__sub(other)
  return Vec(self.x - other.x, self.y - other.y, self.z - other.z)
end

function Vec:__unm()
  return Vec(-self.x, -self.y, -self.z)
end

function Vec:__mul(other)
  if type(other) == 'number' then
    return Vec(self.x * other, self.y * other, self.z * other)
  else
    return Vec(self.x * other.x, self.y * other.y, self.z * self.z)
  end
end

function Vec:__div(other)
  if type(other) == 'number' then
    return Vec(self.x / other, self.y / other, self.z / other)
  else
    return Vec(self.x / other.x, self.y / other.y, self.z / other.z)
  end
end

function Vec:equals(x, y, z)
  return self.x == x and self.y == y and self.z == z
end

function Vec:__eq(other)
  return other and self.x == other.x and self.y == other.y
                                     and self.z == other.z
end

function Vec:floor()
  local floor = math.floor
  return Vec(floor(self.x), floor(self.y), floor(self.z or 0))
end

function Vec:ceil()
  local ceil = math.ceil
  return Vec(ceil(self.x), ceil(self.y), ceil(self.z or 0))
end

function Vec:dot(other)
  return self.x * other.x + self.y * other.y + self.z * other.z
end

function Vec:add(other)
  self.x = self.x + other.x
  self.y = self.y + other.y
  self.z = self.z + other.z
end

function Vec:clamp(max)
  local length = self:length()
  if length >= max then
    self.x = self.x / length * max
    self.y = self.y / length * max
    self.z = self.z / length * max
  end
end

function Vec:length()
  return math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)
end

function Vec:normalized()
  local length = self:length()
  if length >= 0.1 then
    return self / length
  else
    return Vec()
  end
end

return Vec

