
local Dir =require 'common.direction'
local Vec = require 'common.vec'

local Session = require 'pl.class' ()

function Session:_init(raw)
  self.raw = raw
  setmetatable(self.raw.entities_info,
               { __index = function (t, k) return rawget(t, tostring(k)) end })
end

function Session:tileAt(i, j, k)
  local tilemap = self.raw.visible_tilemap
  if i > 0 and j > 0 and k > 0 and i <= tilemap.h and j <= tilemap.w
                                                  and k <= tilemap.d then
    local index = (k - 1) * tilemap.w * tilemap.h + (i - 1) * tilemap.w + j
    return tilemap.tiles[index]
  end
end

function Session:entities()
  return pairs(self.raw.entities_info)
end

function Session:entityName(idx)
  return self.raw.entities_info[idx].name
end

function Session:entityPosition(idx)
  local pos = self.raw.entities_info[idx].maybe_position
  return pos and Vec(pos.x+1, pos.y+1, pos.z+1)
end

function Session:entityAppearance(idx)
  return self.raw.entities_info[idx].appearance
end

function Session:entityPassable(idx)
  return self.raw.entities_info[idx].passable
end

function Session:primaryActionOn(idx)
  return self.raw.entities_info[idx].possible_actions[1]
end

function Session:actionsOn(id)
  return ipairs(self.raw.entities_info[id].possible_actions)
end

function Session:entityID(idx)
  return self.raw.entities_info[idx].id
end

function Session:playerIndex()
  return self.raw.player_state.id
end

function Session:playerID()
  return self:entityID(self:playerIndex())
end

function Session:playerHP()
  return self.raw.player_state.hp, self.raw.player_state.max_hp
end

function Session:playerPosition()
  return self:entityPosition(self:playerIndex())
end

function Session:playerSightRange()
  return self.raw.player_state.sight_range
end

function Session:playerJournal()
  return self.raw.player_state.journal
end

function Session:entitiesAt(pos)
  local indices = {}
  local n = 1
  for id,info in pairs(self.raw.entities_info) do
    local entity_pos = info.maybe_position
    if entity_pos then
      local epos = Vec(entity_pos.x+1, entity_pos.y+1, entity_pos.z+1)
      if pos == epos then
        indices[n] = id
        n = n + 1
      end
    end
  end
  return ipairs(indices)
end

function Session:firstEntityAt(pos)
  for i, idx in self:entitiesAt(pos) do
    if i then return idx end
  end
end

function Session:blockingEntityAt(pos)
  for _, idx in self:entitiesAt(pos) do
    if not self:entityPassable(idx) then
      return idx
    end
  end
end

function Session:allEntitiesWithActions()
  local entities = {}
  local n = 0
  for id, entity in pairs(self.raw.entities_info) do
    if #entity.possible_actions > 0 then
      n = n + 1
      entities[n] = id
    end
  end
  entities.n = n
  return entities
end

function Session:actionRequest(action_name)
  return {
    TryAction = {
      action_name, {
        Self = { Entity = self:playerID() },
      }
    }
  }
end

function Session:targetedActionRequest(action) -- luacheck: no self
  return {
    TryAction = { action['name'], action['parameters'] }
  }
end

function Session:defaultActionRequest(dir)
  local player_pos = self:playerPosition()
  local offset = Vec.fromDir(dir)
  local pos = player_pos + offset
  for _, action in ipairs(self.raw.player_state.actions) do
    local param_pos = Vec(unpack(action['parameters']['Pos'])) + Vec(1,1,1)
    if pos == param_pos then
      return {
        TryAction = {
          action['name'], action['parameters']
        }
      }
    end
  end
end

function Session:moveRequest(dir)
  return {
    TryAction = {
      self.raw.player_state.move_action, {
        Self = { Entity = self:playerID() },
        Dir = { Direction = Dir[dir] }
      }
    }
  }
end

return Session

