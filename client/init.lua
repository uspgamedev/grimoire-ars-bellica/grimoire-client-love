
-- luacheck: globals love

local SafeAccess    = require 'common.safe_access'
local CLASS         = require 'pl.class'

local Client = CLASS(SafeAccess)

function Client:_init(ui, server_stream)
  self.ui = ui
  self.server_stream = server_stream
  self.session = nil
end

function Client:pressKey(key)
  if key == 'q' then
    self.server_stream:send[["EndSession"]]
    love.event.quit()
  else
    self.ui.control:press(key)
  end
end

function Client:releaseKey(key)
  self.ui.control:release(key)
end

function Client:draw()
  self.ui:draw(self.session, love.graphics)
  self.ui.control:reset()
end

return Client

