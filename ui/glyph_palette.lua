
local COLOR = require 'ui.color'
local Glyph = require 'ui.glyph'

return {
  EMPTY = Glyph(COLOR.WHITE, 1, 1),
  ROCK  = Glyph(COLOR.GREY, 18, 11),
  BELOW = Glyph(COLOR.DARK_GREEN, 1, 6),
  ClosedDoor = Glyph(COLOR.GREY, 4, 4),
  OpenDoor = Glyph(COLOR.GREY, 4, 5),
  PLAYER = Glyph(COLOR.WHITE, 1, 28),
  ENEMY = Glyph(COLOR.RED, 9, 32),
  TREASURE = Glyph(COLOR.YELLOW, 5, 23),
  SHARP = Glyph(COLOR.WHITE, 21, 36),
}

