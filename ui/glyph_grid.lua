
local COLOR = require 'ui.color'
local Glyph = require 'ui.glyph'
local CLASS = require 'pl.class'

local GlyphGrid = CLASS()

function GlyphGrid:_init(width, height)
  self.width = width
  self.height = height
  local g = love.graphics -- luacheck: globals love
  local texture = g.newImage("assets/textures/spritesheet.png")
  texture:setFilter('nearest', 'nearest')
  self.batch = g.newSpriteBatch(texture, self.width * self.height)
  self.texrows = texture:getHeight() / Glyph.SIZE
  self.texcols = texture:getWidth() / Glyph.SIZE
  self.quads = {}
  for i = 1, self.texrows do
    for j = 1, self.texcols do
      local x, y = GlyphGrid.tileToScreen(i, j)
      local index = self:tileToQuadIndex(i, j)
      self.quads[index] = g.newQuad(x, y, Glyph.SIZE, Glyph.SIZE,
                                           texture:getDimensions())
    end
  end
  self.bg = {}
  self.fg = {}
  self.texts = {}
  self.blank = self.quads[self:tileToQuadIndex(6, 9)]
  self.empty = self.quads[self:tileToQuadIndex(1, 1)]
  for i = 1, height do
    self.bg[i] = {}
    self.fg[i] = {}
    self.texts[i] = {}
    for j = 1, width do
      self.batch:setColor(0, 0, 0)
      self.bg[i][j] = self.batch:add(self.blank, GlyphGrid.tileToScreen(i, j))
      self.batch:setColor(1, 1, 1)
      self.fg[i][j] = self.batch:add(self.empty, GlyphGrid.tileToScreen(i, j))
      self.texts[i][j] = false
    end
  end
end

function GlyphGrid.tileToScreen(i, j)
  return (j - 1) * Glyph.SIZE, (i - 1) * Glyph.SIZE
end

function GlyphGrid:tileToQuadIndex(i, j)
  return (i - 1) * self.texcols + j
end

function GlyphGrid:set(i, j, glyph, inversed)
  if type(glyph) == 'string' then
    self.texts[i][j] = glyph
  else
    local bg_color = inversed and glyph.color or COLOR.BLACK
    local fg_color = inversed and COLOR.BLACK or glyph.color
    local index = self:tileToQuadIndex(glyph.i, glyph.j)
    self.batch:setColor(bg_color)
    self.batch:set(self.bg[i][j], self.blank,
                   GlyphGrid.tileToScreen(i, j))
    self.batch:setColor(fg_color)
    self.batch:set(self.fg[i][j], self.quads[index],
                   GlyphGrid.tileToScreen(i, j))
  end
end

function GlyphGrid:bindWriter(window)
  local pos = window.pos
  local size = window.size
  return function(i, j, glyph, inversed)
    if i >= 1 and i <= size.y and j >= 1 and j <= size.x then
      self:set(pos.y + i - 1, pos.x + j - 1, glyph, inversed)
    end
  end
end

function GlyphGrid:draw(g)
  g.draw(self.batch, 0, 0, 0, 2, 2)
  g.setColor(COLOR.WHITE)
  g.push()
  g.scale(2, 2)
  for i, row in ipairs(self.texts) do
    for j, text in ipairs(row) do
      if text then
        local x, y = GlyphGrid.tileToScreen(i, j)
        g.print(text, x, y)
        row[j] = false
      end
    end
  end
  g.pop()
end

return GlyphGrid

