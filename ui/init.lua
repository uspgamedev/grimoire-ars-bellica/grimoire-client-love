
local Control           = require 'ui.control'
local Glyph             = require 'ui.glyph'
local GlyphGrid         = require 'ui.glyph_grid'
local MapWindow         = require 'ui.window.map_window'
local JournalWindow     = require 'ui.window.journal_window'
local DescriptionWindow = require 'ui.window.description_window'
local MenuWindow        = require 'ui.window.menu_window'

local Vec = require 'common.vec'

local CLASS = require 'pl.class'

local UI = CLASS()

function UI:_init(graphics)
  self.glyph_grid = GlyphGrid(32, 24)
  self.map_window = MapWindow(Vec(1, 1), Vec(17, 17))
  self.journal_window = JournalWindow(Vec(1, 18), Vec(32, 7))
  self.menu_window = MenuWindow(Vec(18, 10), Vec(15, 8))
  self.description_window = DescriptionWindow(Vec(18, 1), Vec(15, 9))
  self.description_window:show()
  self.font = graphics.newFont('assets/fonts/november.ttf', Glyph.SIZE)
  self.font:setFilter('nearest', 'nearest')
  self.control = Control()
end

function UI:draw(session, graphics)
  graphics.setFont(self.font)
  local writer = self.glyph_grid:bindWriter(self.map_window)
  self.map_window:refresh(session, writer)
  writer = self.glyph_grid:bindWriter(self.journal_window)
  self.journal_window:refresh(session, writer)
  writer = self.glyph_grid:bindWriter(self.description_window)
  self.description_window:refresh(session, writer)
  writer = self.glyph_grid:bindWriter(self.menu_window)
  self.menu_window:refresh(session, writer)
  self.glyph_grid:draw(graphics)
end

return UI

