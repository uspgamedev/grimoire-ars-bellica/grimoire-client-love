
local Window = require 'ui.window'
local GLYPH_PALETTE = require 'ui.glyph_palette'
local Match = require 'common.match'

local CLASS = require 'pl.class'

local MenuWindow = CLASS(Window)

MenuWindow.ITEM = Match()

function MenuWindow:_init(pos, size)
  self:super(pos, size)
  self:reset()
end

function MenuWindow:reset()
  self.confirmed = false
  self.items = {}
  self.n = 0
end

function MenuWindow:addItem(item)
  self.n = self.n + 1
  self.items[self.n] = item
end

function MenuWindow:confirm()
  self.confirmed = true
end

function MenuWindow:text(text)
  self:addItem { TEXT = text }
end

function MenuWindow:selectable(label, selected)
  self:addItem { SELECTABLE = { label = label, selected = selected } }
  return selected and self.confirmed
end

function MenuWindow:refresh(session, grid_writer)
  if session then
    for j = 1, self.size.x do
      grid_writer(1, j, GLYPH_PALETTE.SHARP)
      grid_writer(self.size.y, j, GLYPH_PALETTE.SHARP)
    end
    for i = self.size.y - 2, 1, -1 do
      grid_writer(self.size.y - i, 1, GLYPH_PALETTE.SHARP)
      grid_writer(self.size.y - i, self.size.x, GLYPH_PALETTE.SHARP)
    end
    if self.n > 0 then
      for line, item in ipairs(self.items) do
        MenuWindow.ITEM(item, grid_writer, line)
      end
    end
  end
  self:reset()
end

function MenuWindow.ITEM.onTEXT(text, out_stream, line)
  out_stream(line + 1, 2, text)
end

function MenuWindow.ITEM.onSELECTABLE(selectable, out_stream, line)
  local text
  if selectable.selected then
    text = "> " .. selectable.label
  else
    text = "  " .. selectable.label
  end
  out_stream(line + 1, 2, text)
end

return MenuWindow


