
local CLASS = require 'pl.class'

local Window = CLASS()

function Window:_init(pos, size)
  self.pos = pos
  self.size = size
end

function Window:refresh(state, grid_writer) -- luacheck: no unused
  return error "Abstract method not implemented"
end

return Window

