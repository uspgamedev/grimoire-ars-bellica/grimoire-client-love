
local GLYPH_PALETTE = require 'ui.glyph_palette'
local Window = require 'ui.window'

local CLASS = require 'pl.class'

local DescriptionWindow = CLASS(Window)

function DescriptionWindow:_init(pos, size)
  self:super(pos, size)
  self.visible = false
  self.entity_idx = nil
end

function DescriptionWindow:show()
  self.visible = true
end

function DescriptionWindow:hide()
  self.visible = false
end

function DescriptionWindow:setEntity(idx)
  self.entity_idx = idx
end

function DescriptionWindow:refresh(session, grid_writer)
  if session and self.visible then
    for j = 1, self.size.x do
      grid_writer(1, j, GLYPH_PALETTE.SHARP)
      grid_writer(self.size.y, j, GLYPH_PALETTE.SHARP)
    end
    for i = self.size.y - 2, 1, -1 do
      grid_writer(self.size.y - i, 1, GLYPH_PALETTE.SHARP)
      grid_writer(self.size.y - i, self.size.x, GLYPH_PALETTE.SHARP)
    end
    if self.entity_idx then
      grid_writer(2, 2, session:entityName(self.entity_idx))
      if self.entity_idx == session:playerID() then
        local hp, max_hp = session:playerHP()
        grid_writer(3, 2, ("%d / %d"):format(hp, max_hp))
      end
    else
      grid_writer(2, 2, "")
    end
  end
end

return DescriptionWindow


