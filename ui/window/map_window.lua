
local GLYPH_PALETTE = require 'ui.glyph_palette'
local Window = require 'ui.window'
local Vec = require 'common.vec'

local CLASS = require 'pl.class'

local MapWindow = CLASS(Window)

function MapWindow:_init(pos, size)
  self:super(pos, size)
  self.cursor = nil
end

function MapWindow:setCursor(pos)
  self.cursor = pos
end

function MapWindow:refresh(session, grid_writer)
  if not session then return end
  local player_map_pos = session:playerPosition()
  local camera_map_pos = self.cursor or player_map_pos
  local center_screen_pos = (self.size / 2):ceil()
  local sight_range = session:playerSightRange()
  local k = camera_map_pos.z
  center_screen_pos.z = k
  if self.cursor then self.cursor.z = k end
  for i = 1, self.size.y do
    for j = 1, self.size.x do
      local screen_pos = Vec(j, i, k)
      local map_pos = camera_map_pos + (screen_pos - center_screen_pos)
      local inversed = (self.cursor and self.cursor == map_pos)
      local tile = session:tileAt(map_pos.y, map_pos.x, map_pos.z)
      local glyph
      if not tile then
        glyph = GLYPH_PALETTE.EMPTY
      elseif tile == 'EMPTY' and session:tileAt(i, j, k - 1) == 'ROCK' then
        glyph = GLYPH_PALETTE.BELOW
      else
        glyph = GLYPH_PALETTE[tile]
      end
      local dist = (player_map_pos - map_pos):length()
      if dist > sight_range then
        glyph = glyph:dim()
      end
      grid_writer(i, j, glyph, inversed)
    end
  end
  for id in session:entities() do
    local map_pos = session:entityPosition(id) if map_pos then
      local inversed = (map_pos == self.cursor)
      local appearance = session:entityAppearance(id)
      local glyph = GLYPH_PALETTE[appearance]
      local screen_pos = center_screen_pos + (map_pos - camera_map_pos)
      grid_writer(screen_pos.y, screen_pos.x, glyph, inversed)
    end
  end
end

return MapWindow


