
local GLYPH_PALETTE = require 'ui.glyph_palette'
local Window = require 'ui.window'

local CLASS = require 'pl.class'

local JournalWindow = CLASS(Window)

function JournalWindow:_init(pos, size)
  self:super(pos, size)
end

function JournalWindow:refresh(session, grid_writer)
  if not session then return end
  for j = 1, self.size.x do
    grid_writer(1, j, GLYPH_PALETTE.SHARP)
    grid_writer(self.size.y, j, GLYPH_PALETTE.SHARP)
  end
  local journal = session:playerJournal()
  local n = #journal
  for i = self.size.y - 2, 1, -1 do
    grid_writer(self.size.y - i, 1, GLYPH_PALETTE.SHARP)
    grid_writer(self.size.y - i, 2, journal[n - i + 1] or "")
    grid_writer(self.size.y - i, self.size.x, GLYPH_PALETTE.SHARP)
  end
end

return JournalWindow


