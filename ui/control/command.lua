
local DIR = require 'common.direction'

local _TYPE = {
  MOVE = "Move",
  INSPECT = "Inspect",
  WAIT = "Wait",
  ACT = "Act",
  CONFIRM = "Confirm",
  CANCEL = "Cancel",
}

local function _TAGGED(typename, options)
  local tagged = {}
  for k,v in pairs(options) do
    tagged[k] = { [typename] = v }
  end
  return tagged
end

return {
  MOVE = _TAGGED(_TYPE.MOVE, DIR),
  INSPECT = { [_TYPE.INSPECT] = true },
  WAIT = { [_TYPE.WAIT] = true },
  ACT = { [_TYPE.ACT] = true },
  CONFIRM = { [_TYPE.CONFIRM] = true },
  CANCEL = { [_TYPE.CANCEL] = true },
}

