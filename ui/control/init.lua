
local COMMAND = require 'ui.control.command'
local CLASS = require 'pl.class'

local _ACTION_MAP = {
  h = COMMAND.MOVE.LEFT,
  j = COMMAND.MOVE.DOWN,
  k = COMMAND.MOVE.UP,
  l = COMMAND.MOVE.RIGHT,
  y = COMMAND.MOVE.UPLEFT,
  u = COMMAND.MOVE.UPRIGHT,
  b = COMMAND.MOVE.DOWNLEFT,
  n = COMMAND.MOVE.DOWNRIGHT,
  tab = COMMAND.INSPECT,
  ['.'] = COMMAND.WAIT,
  ['space'] = COMMAND.ACT,
  ['return'] = COMMAND.CONFIRM,
  ['escape'] = COMMAND.CANCEL,
}

local _MODIFIER_MAP = {
  lshift = "Interact",
  lctrl = "Attack"
}

local Control = CLASS()

function Control:_init()
  self.action_pressed = false
  self.modifier = ""
end

function Control:press(key)
  local modifier = _MODIFIER_MAP[key]
  if modifier then
    self.modifier = modifier
  end
  self.action_pressed = _ACTION_MAP[key]
end

function Control:release(key)
  if _MODIFIER_MAP[key] == self.modifier then
    self.modifier = ""
  end
end

function Control:reset()
  self.action_pressed = false
end

function Control:getCommand()
  if self.action_pressed then
    local key, value = next(self.action_pressed)
    return { [self.modifier .. key] = value }
  end
end

return Control

