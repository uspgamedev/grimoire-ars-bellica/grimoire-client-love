
return {
  BLACK = {0, 0, 0},
  WHITE = {1, 1, 1},
  GREY = {.2, .2, .2},
  YELLOW = {0.8, 0.8, 0.3},
  RED = {0.8, 0.3, 0.3},
  DARK_GREEN = {0.1, 0.3, 0.1},
  GREEN = {0.3, 0.8, 0.3}
}

