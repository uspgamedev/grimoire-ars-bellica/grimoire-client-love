
local CLASS = require 'pl.class'

local Glyph = CLASS()

function Glyph:_init(color, i, j)
  self.color = color
  self.i = i
  self.j = j
end

function Glyph:clone()
  return Glyph({ unpack(self.color) }, self.i, self.j)
end

function Glyph:dim()
  local new = self:clone()
  for i, v in ipairs(new.color) do
    new.color[i] = v * 0.5
  end
  return new
end

Glyph.SIZE = 16

return Glyph

