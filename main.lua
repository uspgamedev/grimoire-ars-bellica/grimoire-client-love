
-- luacheck: globals love
local Client  = require 'client'
local Stack   = require 'state.stack'
local UI      = require 'ui'
local SOCKET  = require 'socket'

-- Client data
local _client

-- State
local _stack

local function _connectToServer()
  local server_stream = SOCKET.tcp()
  server_stream:settimeout(1/60)
  print(server_stream:connect('localhost', '1337'))
  return server_stream
end

function love.load()
  local ui = UI(love.graphics)
  local server_stream = _connectToServer()
  _client = Client(ui, server_stream)
  _stack = Stack()
  _stack:push('play')
end

function love.keypressed(key)
  _client:pressKey(key)
end

function love.keyreleased(key)
  _client:releaseKey(key)
end

function love.update(dt)
  _stack:update(_client, dt)
end

function love.draw()
  _client:draw()
end

